//
//  WeatherManager.swift
//  Clima
//
//  Created by RAMKIRAN on 3 - 03 - 2017.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import Foundation
import UIKit

struct WeatherManager {
    let weatherURL = "http://api.openweathermap.org/data/2.5/weather?appid=d1a76d27149c34ab65cd31fc683ecefa&units=metric"
    func fetchWeather(cityname: String) {
        let urlString = "\(weatherURL)&q=\(cityname)"
        print(urlString)
    }
    
    func performRequest(urlString: String) {
        //Create URL
        if let url = URL(string: urlString) {
//            Create URL session
            let session = URLSession(configuration: .default)
//            Create Task
            let task = session.dataTask(with: url, completionHandler: <#T##(Data?, URLResponse?, Error?) -> Void#>)
//            STart Task
            task.resume()
        }
    }
    
    func handle(data: Data?, response: URLResponse?, error: Error?) {
        
    }
}
